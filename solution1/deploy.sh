set -e

aws cloudformation validate-template --template-body file://template.yml

STACK_NAME="WedoogiftDevOpsChallenge"
OPERATION="create-stack"
# If the stack exists, we update it
aws cloudformation describe-stacks --stack-name "$STACK_NAME" 2>&1 > /dev/null && OPERATION="update-stack"

aws cloudformation "$OPERATION" --template-body file://template.yml --stack-name $STACK_NAME --parameters "ParameterKey=Environment,ParameterValue=prod" "ParameterKey=SourcePrincipalId,ParameterValue=arn:aws:iam::923373297020:root" --capabilities CAPABILITY_IAM CAPABILITY_NAMED_IAM