# Wedoogift DevOps challenge - Solution 1 - cheaper

## Architecture

![diagram](diagram.png)

## Details

This solution is a very simple and cheap solution to store statistics about searched values.

I've assumed here that the backend is running on ECS (as far as I've understood from the job description).

## Usage

Send the searched values with the following format in the queue:

```
{"searchedValue": "my super unknown brand"}
```

## Improvements

This code could have been improved by:

- Using AWS CDK to deploy the stack
- Add encryption
- Add an API instead of the SQS queue directly (to improve compatibility and reduce dependency to AWS APIs)
