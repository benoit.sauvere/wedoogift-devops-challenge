set -e

QUEUE_URL=$(aws cloudformation describe-stacks --stack-name WedoogiftDevOpsChallenge --query "Stacks[0].Outputs[?OutputKey=='SearchedValuesAnalyticsQueueUrl'].OutputValue" --output text)

aws sqs send-message --queue-url "$QUEUE_URL" --message-body '{"searchedValue": "my super unknown brand"}'