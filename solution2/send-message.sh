set -e

DELIVERY_STREAM_NAME=$(aws cloudformation describe-stacks --stack-name WedoogiftDevOpsChallenge2 --query "Stacks[0].Outputs[?OutputKey=='AnalyticsSearchesDeliveryStreamName'].OutputValue" --output text)

aws firehose put-record --delivery-stream-name "$DELIVERY_STREAM_NAME" --record '{"Data": "{\"searchedValue\": \"my super unknown brand\",\"eventtime\": \"2021-10-10T17:05:40Z\",\"userid\": \"fc810efd-a35a-4b83-bf5b-3dab909c6c38\"}"}'