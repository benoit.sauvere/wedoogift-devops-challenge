set -e

aws cloudformation validate-template --template-body file://template.yml

STACK_NAME="WedoogiftDevOpsChallenge2"
OPERATION="create-stack"
# If the stack exists, we update it
(aws cloudformation describe-stacks --stack-name "$STACK_NAME" 2>&1 > /dev/null) && OPERATION="update-stack"

aws cloudformation "$OPERATION" --template-body file://template.yml --stack-name $STACK_NAME --parameters "ParameterKey=Environment,ParameterValue=prod" --capabilities CAPABILITY_IAM CAPABILITY_NAMED_IAM