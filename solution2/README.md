# Wedoogift DevOps challenge - Solution 2 - more flexible

## Architecture

![diagram](diagram.png)

## Details

This solution is a more flexible than the first one, as you cann ad dmetadata with the searched value and perform some advances queries on it using AWS Athena. This has a higher cost and latency as you athena can be quite expensive with large datasets.

I've assumed here that the backend is running on ECS (as far as I've understood from the job description).

## Data format

```
{
    "searchedValue": "my super unknown brand",
    "eventtime": "2021-10-10T17:05:40Z",
    "userid": "fc810efd-a35a-4b83-bf5b-3dab909c6c38"
}
```

The Glue table is configured with this format. Please adjust it in case you want to add / remove fields.

## Transformation lambda

When you push JSONs to the kinesis firehose, they are stored without any line break:

```
{"a": 1} {"a": 2} {"a": 3}
```

In order to work with Athena, we need to add a line break using a firehose transformation lambda. This will render:

```
{"a": 1}
{"a": 2}
{"a": 3}
```

## Request data

You can find an example request in the AWS Athena console under the "Saved queires" tab.

For instance you can count all the occurences of the searched values with the following query (replace database name):

```
SELECT searchedvalue, COUNT(*) FROM "analyticsdatabase-iwygd4wcuykl"."wedoogift-analytics-prod-search-table"
WHERE searchedvalue != ''
GROUP BY searchedvalue;
```

## Send fake data

You can use the `send-message.sh` script to send fake data to the firehose data stream.
